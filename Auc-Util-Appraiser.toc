## Title: Auc:Util:Appraiser
## Interface: 80205
## Dependencies: Stubby, Auc-Advanced
##
## Version: <%version%> (<%codename%>)
## Revision: $Id$
## Author: Norganna's AddOns
## X-Part-Of: Auctioneer
## X-Category: Auction House
## X-Max-Interface: 80100
## X-URL: http://auctioneeraddon.com/
## X-Feedback: http://forums.norganna.org
##
Embed.xml
